#ifndef VISUALELEMENT_H
#define VISUALELEMENT_H

#include <QString>

struct VisualElement {
    QString id;
    QString text;
};

#endif // VISUALELEMENT_H
