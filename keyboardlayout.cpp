#include "keyboardlayout.h"


KeyboardLayout::KeyboardLayout(QObject* parent)
    : _isFirstLayout(true)
    , QObject(parent)
{

}

void KeyboardLayout::reconstructKeyboardModel()
{
    _isFirstLayout = !_isFirstLayout;
    QVector<const VisualElement*> testLayout = _createTestVisualElementsLayout();
    emit keyboardLayoutChanged(testLayout, 4, 7);

    layoutSwappingTimer.start(2500);
}

QVector<const VisualElement*> KeyboardLayout::_createTestVisualElementsLayout()
{
    QString testLayoutData;
    if (_isFirstLayout) {
        testLayoutData = "1234567890qwertyuiopasdfghjk";
    }
    else {
        testLayoutData = "abcdefg";
    }
    QVector<const VisualElement*> testLayout;
    for (int i = 0; i < testLayoutData.length(); ++i){
        testLayout.append(new VisualElement{QString::number(i), testLayoutData.mid(i, 1)});
    }
    return testLayout;
}
