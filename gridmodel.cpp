#include "gridmodel.h"


GridModel::GridModel(QObject* parent)
    : QAbstractListModel(parent)
{
    connect(&(this->reloadTimer), SIGNAL(timeout()),
            this,SLOT(loadModel()));
    _elements.append(new VisualElement{"3","4"});
    this->reloadTimer.start(2500);
}


int GridModel::rowCount(const QModelIndex& parent) const
{
    if (parent.isValid()) {
        return 0;
    }
    return _elements.length();
}

QVariant GridModel::data(const QModelIndex& index, int role) const
{
    if ((!index.isValid()) || (this->rowCount() <= index.row())) {
        return QVariant();
    }

    switch (role) {
    case Qt::DisplayRole:
        return QVariant(_elements[index.row()]->text);
        break;
    default:
        return QVariant();
    }
}

QHash<int, QByteArray> GridModel::roleNames() const
{
    QHash<int, QByteArray> roles;
    roles[Qt::DisplayRole] = "display";
    return roles;
}

void GridModel::loadModel()
{
    qDebug() << "loadModel() started";
    this->beginResetModel();
    qDebug() << "string before code line" << "\"this->endResetModel();\" ";
    this->endResetModel();
    qDebug() << "string after code line" << "\"this->endResetModel();\" ";
}

QModelIndex GridModel::_getQModelIndexOnRoot()
{
    return QModelIndex();
}
