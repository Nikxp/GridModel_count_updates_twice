#ifndef KEYBOARDLAYOUT_H
#define KEYBOARDLAYOUT_H

#include <QObject>
#include <QVector>
#include <QTimer>

#include "visualelement.h"

class KeyboardLayout : public QObject
{
    Q_OBJECT
public:
    QTimer layoutSwappingTimer;

    explicit KeyboardLayout(QObject *parent = 0);


signals:
    void keyboardLayoutChanged(const QVector<const VisualElement*>& keyboardModel, int numberOfRows, int numberOfColumns);


public slots:
    void reconstructKeyboardModel();

private:
    bool _isFirstLayout;

    QVector<const VisualElement*> _createTestVisualElementsLayout();
};

#endif // KEYBOARDLAYOUT_H
