#ifndef INITIALIZER_H
#define INITIALIZER_H

#include "gridmodel.h"
#include "keyboardlayout.h"

#include <QGuiApplication>
#include <QQmlContext>

class Initializer : public QObject
{
    Q_OBJECT
public:
    explicit Initializer(QQmlContext* context, QObject* parent = 0);
    ~Initializer();

private:

    KeyboardLayout* _keyboardLayout;
    GridModel* _keyboardModel;

};

#endif // INITIALIZER_H
