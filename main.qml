import QtQuick 2.7
import QtQuick.Controls 2.1
import QtQuick.Layouts 1.3

ApplicationWindow {
    visible: true
    width: 700
    height: 480
    title: qsTr("Virtual Keyboard")

    Rectangle {
        objectName: "keyboardLayout"
        height: parent.height*0.47
        width: parent.width
        anchors.bottom: parent.bottom
        color: "black"

        GridView {
            id: inputPanel

            height: parent.height * 0.8
            width: parent.width
            anchors.bottom: parent.bottom
            anchors.left: parent.left

            cellHeight: 70
            cellWidth: 200

            model: keyboardModel
            delegate: KeyButton {
                width: inputPanel.cellWidth
                height: inputPanel.cellHeight
                displayedText: model.display
            }
            onCountChanged:
            {
                console.log(inputPanel.count)
            }
        }
    }
}
