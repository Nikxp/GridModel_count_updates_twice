#include <QGuiApplication>
#include <QQmlApplicationEngine>

#include "initializer.h"

int main(int argc, char *argv[])
{
    QGuiApplication application(argc, argv);

    QQmlApplicationEngine engine;
    QQmlContext* context = engine.rootContext();
    Initializer initializer(context);

    engine.load(QUrl(QLatin1String("qrc:/main.qml")));

    return application.exec();
}
