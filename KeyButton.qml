import QtQuick 2.0

Item {
    id: item
    property string displayedText
    property var buttonId

    Rectangle {
        anchors.fill: parent
        color: "#383533"
        radius: 5
        anchors.margins: 2.5
        Text {
            text: displayedText
            anchors.centerIn: parent
            color: "white"
        }
    }
}
