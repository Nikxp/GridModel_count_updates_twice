#include "initializer.h"

Initializer::Initializer( QQmlContext* context, QObject* parent)
    : QObject(parent)
{
    _keyboardModel = new GridModel();
    context->setContextProperty("keyboardModel", _keyboardModel);

}

Initializer::~Initializer()
{
    delete(_keyboardModel);
}


