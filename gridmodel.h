#ifndef GRIDMODEL_H
#define GRIDMODEL_H

#include <QAbstractTableModel>
#include <QQuickItem>
#include <QString>
#include <QDebug>
#include <QTimer>

#include "visualelement.h"

class GridModel : public QAbstractListModel
{
    Q_OBJECT
public:
    explicit GridModel(QObject* parent = 0);

    int rowCount(const QModelIndex& parent = QModelIndex()) const override;
    QVariant data(const QModelIndex& index, int role) const override;
    QHash<int, QByteArray> roleNames() const override;

public slots:
    void loadModel();

private:

    QVector<const VisualElement*> _elements;
    QTimer reloadTimer;

    QModelIndex _getQModelIndexOnRoot();
};

#endif // GRIDMODEL_H
